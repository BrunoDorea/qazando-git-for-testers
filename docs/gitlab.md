# GitLab

## Criar um novo projeto em branco

## Subir um projeto local para o repositorio

## Criar uma nova branch

```bash
git checkout -b feature/task-0001
```

## Alterando o projeto e criando a nova branch no repositorio

```bash
git add .

git push --set-upstream origin feature/task-0001
```

## Criar uma nova branch no repositorio do GitLab

## Comando fetch

```bash
git fetch
git checkout feature/task-0002
```

## Abrindo um merge request

```bash
git add .
git commit -m "ajuste novo"
git push
```

No GitLab

Criar 'novo merge request'
