# Lista de Comandos

## git status

geralmente usado para demonstrar o que foi modificado dentro da pasta/projeto que você está alterando

## git init

geralmente usado para demonstrar que um pacote/projeto vai ser iniciado o versionamento com os comandos git

## git merge

geralmente usado para realizar um merge entre 2 branches separadas

## git checkout -b nomeDaBranch

geralmente usado para criar uma nova branch e fazer a navegação para a mesma

## git fetch

geralmente usado para realizar refresh de todas as branches criadas no repositorio

## git checkout nomeDaBranch

geralmente usado para modificar da sua branch local para a branch no qual você está passando no comando

## git commit -m "descrição"

geralmente usado para descrever o que você está subindo para o repositorio

## git push

geralmente usado para subir de fato as alterações para a branch que você está e tornar seu código visível para todos

## git luu

geralmente usado para atualizar a branch que você está
