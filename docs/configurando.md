# Configurando o Git

## Baixar o git e instalar o git

[Git](https://git-scm.com/downloads)

## Validando instalação

```bash
git --version
```
