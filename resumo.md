# Curso de Git para Testers - QAzando

## Lista de Comandos

[Lista de Comandos](./docs/comandos.md)

## O que significa branch?

Em tradução literal, significa 'ramo'. Ela é uma ramificação do seu projeto. Os repositorios no GitHub funcionam como uma árvore

## Configurando o Git

[Configurando o Git](./docs/configurando.md)

## GitLab

[GitLab](./docs/gitlab.md)
